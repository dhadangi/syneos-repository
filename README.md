Investigator Update

	Description
		This is an LCA that checks Hospital name attribute at HCP entity and if the values of this attribute gets updated, then it also sets value of Public Domain Data attribute.
		
	#change log
	Last Update Date: 10/13/2018
	Version: 1.0.0
	Description: Initial version