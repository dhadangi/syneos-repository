package syneos_reltio.lca_investigatorpublicdomainupdate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import com.reltio.lifecycle.framework.ActionType;
import com.reltio.lifecycle.framework.IAttributeValue;
import com.reltio.lifecycle.framework.IAttributes;
import com.reltio.lifecycle.framework.ILifeCycleObjectData;
import com.reltio.lifecycle.framework.IObject;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.framework.ISimpleAttributeValue;
import com.reltio.lifecycle.framework.LifeCycleActionBase;

public class CheckInvestigatorUpdate extends LifeCycleActionBase {
	static final String PDValFlag = "true";
	@Override
	public ILifeCycleObjectData beforeSave(IReltioAPI reltioAPI, ILifeCycleObjectData data) {
		if(ActionType.CREATE.equals(data.getActionType()))  {
			return null;
		}	
		IObject object = data.getObject();
		IAttributes attributes = object.getAttributes();
		try {
				Map<String, Object> attributeUpdateDates = (Map<String, Object>) object.getCrosswalks().getCrosswalks().get(0).toMap().get("singleAttributeUpdateDates");
				if (isAttributeUpdated("SHCompanyHospitalInstitution", attributeUpdateDates)) {
					List <IAttributeValue> hospitalName = attributes.getAttributeValues("SHCompanyHospitalInstitution");
					IAttributeValue getPDFlagVal = getAttrVal(attributes.getAttributeValues("INV_PD_VAL_FLAG"));
					if (!hospitalName.isEmpty() && (getPDFlagVal == null))	{	
						ISimpleAttributeValue newValue = attributes.createSimpleAttributeValue("INV_PD_VAL_FLAG").value("false").build();
						attributes.addAttributeValue(newValue);
						object.getCrosswalks().getCrosswalks().get(0).getAttributes().addAttribute(newValue);
					}
					else if (!hospitalName.isEmpty() && (getPDFlagVal.getValue().equals(PDValFlag)))	{	
						ISimpleAttributeValue pdFlag =  (ISimpleAttributeValue) getAttrVal(attributes.getAttributeValues("INV_PD_VAL_FLAG"));
						pdFlag.setValue("false");
					}
				}
				return data;	
		}catch(Exception ex) {
			throw new RuntimeException("LCA: CheckInvestigatorUpdate: an error encounters while executing the custom method for public domain flag update \n",ex);
		}
	}

	private boolean isAttributeUpdated(String attributeName, Map<String, Object> attributeUpdateDates) throws ParseException {
		Object[] objectArr = attributeUpdateDates.values().toArray();
		if (objectArr.length > 0) {
			String checkStr = getLatestDate(objectArr);
			if (checkStr.length() > 0) {
				for (Map.Entry<String, Object> entry : attributeUpdateDates.entrySet()) {
					if (entry.getValue().toString().contentEquals(checkStr)) {
							if (entry.getKey().contains(attributeName)) {
								return true;
							}
						}
					}
			}
		}
		return false;
	}
	
	private String getLatestDate (Object[] arrInp) throws ParseException {
		String returnVal = "";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		long returnValinNum = 0, TempValinNum = 0;
		int index = 0;
		while(index < arrInp.length) {
			if (returnVal.isEmpty()) {
				returnVal 		= arrInp[index].toString();
				returnValinNum	= format.parse(returnVal).getTime();
			}
			else {
				String tempStr = arrInp[index].toString();
				TempValinNum	= format.parse(tempStr).getTime();
				if (returnValinNum < TempValinNum) {
					returnVal = tempStr;
					returnValinNum = TempValinNum;
				}
			}
			++index;
		}
		return returnVal;
	}
	
	private IAttributeValue getAttrVal (List <IAttributeValue> attrValues) {
		IAttributeValue resultAttrValue = null;
        for (IAttributeValue attrValue: attrValues) {
			if (attrValue.isOv()) {
				resultAttrValue =  attrValue;
	        	break;
	    	}
        }
        return resultAttrValue;
	}
	
}
